<?php require __DIR__. './__connect_db.php' ?>
<?php 

$page_name='compare';

//取得資料
if(! empty($_SESSION['compare'])){
$detail = array_keys($_SESSION['compare']);
$b_sql = sprintf("SELECT * FROM `lunggage_data` ld JOIN `product_list` pl ON ld.`SID`= pl.`type_sid` JOIN `color_mapping` cm ON cm.`color_sid`=pl.`color_sid`  WHERE pl.`sid`  IN ('%s') ",implode("','", $detail));
$b_stmt = $pdo->query($b_sql);
$detail_data = [];
while($a = $b_stmt->fetch(PDO::FETCH_ASSOC)){

    $detail_data[$a['SID']] = $a;
    }
}

// header('Content-Type: text/plain');
// print_r($_SESSION['compare']);
// print_r($detail_data);
// exit;

?>
<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>比較商品</title>
    <link href="https://fonts.googleapis.com/css?family=Charmonman|Noto+Sans+TC" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="./css/navigation.css">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');

        html {
            font-size: 16px;
        }

        body {
            /*
            font-family: 'Marko One', serif;
            font-family: 'Mukta Malar', sans-serif;
            font-family: 'Merriweather', serif;
            */
            font-family: 'Noto Sans TC', sans-serif;
            font-size: 1rem;
            color: #4d5258;
        }

        .wrapper {
            flex-direction: column;
        }

        /* ---------------------------Wawa tamp */
        .con-1440 {
            max-width: 75%;
            margin: 0 auto;
        }

        /* ---------special compare tamp */
        .c-li-lineh {
            line-height: 1.2rem;
            list-style-position: outside;
            margin-left: 20px;
            padding-top: 10px;
        }
        .delete_btn{
            top: 0;
            right: 0;
        }

        /* ----------------------section top */
        .top_img {
            width: 100%;
            height: 350px;
            background: url("./images/compare_bg.jpg") center center no-repeat;
            background-size: cover;
            /* background-attachment: fixed; */
        }

        .compare_top {
            padding: 40px 0;
        }

        .square {
            width: 190px;
            height: 190px;
        }

        .compare_title .inner-text {
            font-size: 4rem;
            border-top: 5px solid #818E9B;
            border-bottom: 5px solid #818E9B;
            /* margin-right: 32px; */
            margin-top: 104px;
            padding: 25px 0;
            font-weight: 700;
            color: #818E9B;
            /* border: 1px solid #f00; */
        }

        .compare_item {
            width: 25%;
            margin-left: 48px;
            padding: 32px;
            min-width: 130px;
            flex: 1;
            /* border: 1px solid #f00; */
        }

        .compare_item img {
            width: 100%;
        }

        .compare_item .p1 {
            font-size: 1.5rem;
            margin-bottom: 4px;
        }

        .compare_item .p2 {
            height: 50px;
        }

        .compare_item .btn {
            width: 75%;
            border: 1px solid #818E9B;
            border-radius: 1.6px;
            padding: 5px;
            margin: 16px auto;
            cursor: pointer;
        }

        .compare_item .btn1 {
            margin-top: 33px;
            background: #818E9B;
            color: #fff;
        }

        /* ----------------------section bottom */
        .compare_bottom {
            padding: 40px 0;
            flex-direction: column;
        }

        .border-line {
            border: 1px solid #ccc;
            width: 90vw;
            margin: 0 auto;
        }

        .compare_m-l {
            width: 25vw;
            margin-left: 90px;
            padding: 32px;
            min-width: 130px;
        }
        /* 測試背景線 */
        .hover-line {
            width: 100%;
            height: 32px;
            background: #ccc;
            top: 90px;
            z-index: -2;
        }

        /* ----------------------- */
        .line {
            line-height: 3rem;
            flex: 1;
            width: 100%;
        }

        .line:hover {
            background: #ccc;
        }

        .info_pdt {
            min-width: 190px;
            /* border: 1px solid #f00; */
        }

        .blank {
            width: 85%;
        }

        .compare_data {
            width: 25%;
            margin-left: 48px;
            padding: 0 32px;
            min-width: 130px;
            flex: 1;
            /* border: 1px solid #f00; */
        }

        .compare_data ul {
            padding-top: 10px;
        }

        /* ------------------------手機版顯示關閉調整區 */
        .mobile_flex {
            display: none;
        }

        .compare_cate {
            display: none;
        }



        /* -------------------------------------------------compare RWD */
        @media screen and (max-width:1120px) {
            .compare_title {
                left: -20px;
            }

            .compare_item {
                margin-left: 24px;
            }

            .compare_data {
                margin-left: 24px;
            }
        }

        @media screen and (max-width:975px) {
            .compare_title {
                min-width: 100px;
                left: 0;
                margin: 0 auto;
            }

            .square {
                width: 100px;
                height: 100px;
            }

            .rwd1 {
                max-width: 90%;
            }

            .compare_title .inner-text {
                font-size: 2.5rem;
                margin-top: 70px;
                padding: 15px 0;
            }

            .line {
                min-width: 100px;
                left: 0;
                margin: 0 auto;
            }

            .line:hover {
                background: transparent;
            }

            .info_pdt {
                text-align: left;
                min-width: 100px;
                width: auto;
            }
        }

        @media screen and (max-width:830px) {
            .top_img {
                height: 250px;
            }

            .compare_top {
                flex-direction: column;
            }

            .compare_item {
                width: 33%;
                margin-left: 12px;
            }

            .rwd_item.rwd1 {
                max-width: 100%;
            }

            .square {
                width: 500px;
                height: 100px;
            }

            .compare_title .inner-text {
                font-size: 2.5rem;
                margin-top: 70px;
                padding: 15px 0;
                border-left: 5px solid #818E9B;
                border-right: 5px solid #818E9B;
                border-top: none;
                border-bottom: none;
            }

            /* ------------bottom */
            .compare_bottom {
                padding: 20px 0;
            }

            .compare_cate {
                display: flex;
            }

            .data_area p {
                line-height: 3rem;
                border-bottom: 1px solid #4d5258;
            }

            .data_area p::after {
                content: '       v        ';
                font-size: 18px;
            }

            .line {
                flex-direction: column;
            }

            .info_pdt {
                text-align: center;
                margin: 0 auto;
                width: 100%;
                background: rgb(223, 234, 245);
                /* display: none; */
            }

            .blank {
                width: 100%;
                flex-direction: row;
            }

            .compare_data {
                width: 33%;
                margin-left: 12px;
            }

        }

        @media screen and (max-width:630px) {
            .rwd1 {
                max-width: 100%;
            }

            .mobile_none {
                display: none;
            }

            .mobile_flex {
                display: flex;
            }

            .top_img {
                height: 150px;
            }

            .fixedmenu{
                position: fixed;  
                top: 0px;  
                z-index: 98;  
            }
            .compare_top {
                /* position: relative; */
                top: 60px;
                background: #fff;
                z-index: 5;
                padding: 10px;
            }

            .compare_item {
                margin: 0;
                padding: 8px;
                min-width: 80px;
            }

            .compare_title {
                align-items: flex-start;
                width: 2rem;
            }

            .compare_title .inner-text {
                width: 2rem;
                font-size: 2rem;
                border: 2px solid #ccc;
                border-radius: 5px;
                padding: 10px;
                margin-top: 10px;
            }

            .compare_icon img {
                width: 30px;
                height: 30px;
                margin: 0 auto;
            }

            /* ------------bottom */
            .compare_data {
                margin: 0;
                padding: 8px;
                min-width: 80px;
                text-align: center;
            }
            @media screen and (max-width:519px){
                .compare_icon img{
                    width: 20px;
                    height: 20px;
                }
            }
        }
    </style>
</head>

<body>
<?php include __DIR__. './__navbar.php' ?>
    <?php //--------------------start----------------?>
    <?php if(empty($detail_data)): ?>
    <?php $url = "../20190115_.container_commdoity/index_commodity.php"; ?> 
    <meta http-equiv="refresh" content="3;url=<?php echo $url; ?>">
        <div class="empty">
            尚未加入商品進入比較清單，3秒後將前往商品列表頁。
        </div>
    <?php else: ?>
    <div class="top_img relative"></div>
    <section>
        <div class="wrapper relative">
            <div class="compare_top d-flex con-1440 justify-center rwd1">
                <div class="compare_title text-center relative mobile_none justify-center rwd1">
                    <div class="square">
                        <p class="inner-text">商品比較</p>
                    </div>
                </div>
                <!--比較商品上方三項 手機版會顯示上面兩個compare_item-->
                <!-- php撈資料 -->
                
                <div class="rwd_item d-flex relative justify-center rwd1">

                <?php //$row1 = $b_stmt->fetch(PDO::FETCH_ASSOC) ?>
                <?php foreach ($detail as $a){
                            $compare_item = $detail_data[$a] ?>
                    <div class="compare_item text-center relative product<?= $compare_item['SID'] ?>">
                    <a class="nonstyle-a delete_btn absolute" style="color:black;" href="javascript: remove_item(<?= $compare_item['SID'] ?>)">&times;</a>
                        <img src="./images/product/<?= $compare_item['pic_nu'] ?>" alt="">
  
                        <p class="p1 product<?= $compare_item['SID'] ?>"><?= $compare_item['brand'] ?></p>
                        
                        <p class="p2 product<?= $compare_item['SID'] ?>"><?= $compare_item['size_text']?></p>
                        <div class="btn btn1 mobile_none">加入購物車</div>
                        <div class="btn mobile_none">加入願望清單</div>
                        <div class="mobile_flex compare_icon">
                            <img src="./images/icon_sm_shopbag.svg" alt="">
                            <img src="./images/icon-love-1.svg" alt="">
                        </div>
                    </div>
                    <?php } ?>
                    
                    <!-- <div class="compare_title text-center relative mobile_flex justify-center align-item-center">
                        <p class="inner-text p-0 m-0">商品比較</p>

                    </div> -->
                    

                </div>
            </div>

            
            <div class="border-line"></div>
            <!-- 橫列為一組div 撈資料時要各自填入不同項商品 -->
            <!-- TODO::不同商品要怎麼使用php撈? 待解決 -->
            <div class="compare_bottom d-flex relative con-1440 rwd1">
                <div class="data_area justify-center">
                    <p class="p-0 m-0 compare_cate justify-center slide-toggle">基礎資訊</p>
                    <div class="line d-flex relative justify-center">
                        
                        <div class="info_pdt text-center">價格</div>
                        
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?= $compare_item['price'] ?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                        
                    </div>

                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">尺寸</div>
                        
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?= $compare_item['size_text'] ?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                        
                    </div>
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">款式</div>
                        
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?=$compare_item['border']?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>

                    </div>
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">容量</div>
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?=$compare_item['insideL']?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="data_area">
                    <p class="p-0 m-0 compare_cate justify-center slide-toggle">詳細規格</p>
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">重量</div>
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?=$compare_item['kg']?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">箱體</div>

                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?=$compare_item['box']?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">材質</div>
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?=$compare_item['texture']?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">長寬高</div>
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?=$compare_item['size']?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">海關鎖</div>
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?=$compare_item['tsa']?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">輪子</div>
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?=$compare_item['roll']?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                    </div>
                    </div>
                <div class="data_area">
                    <p class="p-0 m-0 compare_cate justify-center slide-toggle">特色功能</p>
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">擴充</div>
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?= $compare_item['expan']?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">煞車</div>
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>"><?= $compare_item['brake']?></div>
                            <div class="compare_title mobile_flex"></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="line d-flex relative justify-center">
                            <div class="info_pdt text-center">保固</div>
                            <div class="blank d-flex justify-center">
                            <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                                <div class="compare_data product<?= $compare_item['SID'] ?>"><?=$compare_item['warranty']?></div>
                                <div class="compare_title mobile_flex"></div>
                                <?php } ?>
                            </div>
                        </div>
                    <!-- 最後一個區塊為文字型 li 或者只接寫好class放資料庫 -->
                    <div class="line d-flex relative justify-center">
                        <div class="info_pdt text-center">其他細節</div>
                        
                        <div class="blank d-flex justify-center">
                        <?php foreach ($detail as $k){
                            $compare_item = $detail_data[$k] ?>
                            <div class="compare_data product<?= $compare_item['SID'] ?>">
                                <ul class="p-0 m-0">
                                <?=$compare_item['anno']?>
                                    <!-- <li class="c-li-lineh">雙TSA安裝鎖</li>
                                    <li class="c-li-lineh">1格拉鍊袋</li>
                                    <li class="c-li-lineh">4個嵌入式雙旋轉輪</li>
                                    <li class="c-li-lineh">伸縮手柄</li> -->
                                </ul>
                            </div>
                            <?php } ?>
                            
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php endif; ?>
    <?php //----------------------end--------------?>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script>
        ////navigation 商品選單開關
        $(".menu_product_R").mouseover(function () {
            $(".product_menuframe_R").css('display', 'flex');
            $(".product_menuframe_R").mouseover(function () {
                $(this).css('display', 'flex');
            });
        });

        $(".product_bar").mouseleave(function () {
            $(".product_menuframe_R").css('display', 'none');
            $(".product_menuframe_R").mouseleave(function () {
                $(this).css('display', 'none');
            });
        });
        //navigation 會員選單開關
        $(".menu_member_R").mouseover(function () {
            $(".member_menuframe_R").css('display', 'flex');
            $(".member_menuframe_R").mouseover(function () {
                $(this).css('display', 'flex');
            });
        });

        $(".member_bar").mouseleave(function () {
            $(".member_menuframe_R").css('display', 'none');
            $(".member_menuframe_R").mouseleave(function () {
                $(this).css('display', 'none');
            });
        });

        //navigation 搜尋
        $('.search').click(function(){
            $('.search_slide').animate({
                width: '250px'
            },300);
        });
        
        
        
        //navigation 手機板menu展開滑動右向左
        $('.nav_menu').click(function(){
            $('#click_menu').css('display', 'block');
            $('.menu_page').animate({
                width: '100%'
            }, 300);
        });
        //navigation 手機板menu關閉
        $('#close_menu').click(function(){
            $('.menu_page').animate({
                width: '0'
            }, 300);
            setTimeout(function(){
                $('#click_menu').css('display','none');
            },400,);
        });

        //比較商品置頂 
        //TODO::目前會因為fixed 原本連接在下面的div會直接往上補
        // var compareTop= $(".compare_top"); 
        // var win=$(window); 
        // var sc=$(document);
        // win.scroll(function(){
        //     if(sc.scrollTop()>=100){  
        //         compareTop.addClass("fixedmenu"); 
        //         compareTop.css('padding', '0, 10px');
        //         $('.compare_item').css('padding', '0, 8px');
        //     }else{  
        //         compareTop.removeClass("fixedmenu");
        //         compareTop.css('padding', '10px');
        //         $('.compare_item').css('padding', '8px');
        //     }
        //     });

        // 比較項目滑開關閉
        $('.slide-toggle').click(function () {
            $(this).siblings().slideToggle(500);
        });

        //移除項目
        var remove_item = function(sid){
            $('.product'+sid).remove();
            $.get('add_to_compare_api.php', {sid:sid}, function(data){
                location.reload();
                cart_count(data);
                $('.product'+sid).remove();
                location.reload();
            }, 'json');
        };
        // var remove_item = function(sid){
        //         $('.product'+sid).remove();
        //     };
    </script>
</body>

</html>