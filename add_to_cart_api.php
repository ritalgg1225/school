<?php

session_start();

if(! isset($_SESSION['cart'])){
    $_SESSION['cart'] = [];
}

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;
$qty = isset($_GET['qty']) ? intval($_GET['qty']) : 0;

if(! empty($sid)){
    // 如果 sid 不為 0, 有設定時

    if(empty($qty)) {
        // 移除
        unset($_SESSION['cart'][$sid]);


    } else {
        // 加入 或 變更
        // TODO: 要去資料庫確認有這個商品, 而且商品在架上

        if($_GET['add']){
            //如果add=true, 那數量顯示就會變成是累積加上
            if(!array_key_exists($sid, $_SESSION['cart'])){
                $_SESSION['cart'][$sid] = 0;
            }
            $_SESSION['cart'][$sid] += $qty; // $_SESSION['cart'][$sid] = $qty + $_SESSION['cart'][$sid];
        } else {
            //如果add=true, 那數量顯示就會變成是直接多少
            $_SESSION['cart'][$sid] = $qty;    
        }
        
    }

}

echo json_encode($_SESSION['cart']);

header('Content-Type: text/plain');
//print_r($_SESSION);