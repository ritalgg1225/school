<?php
require __DIR__. '/__connect_db.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '參數不足',
    'postData' => [],
];

if(isset($_GET['sid'])){
    $sid = $_GET['sid'];
    $user_id = $_SESSION['user']['id'];
    // $email = strtolower(trim($_POST['email']));
    // $password = sha1(trim($_POST['password']));

    $sql = "SELECT * FROM `wishlist` WHERE `member_id`=? AND `list_id`=?";

    //url裡有沒有add關鍵字
    if(isset($_GET['add'])) {
        $add = $_GET['add'];
        if($add=="true") { //要用字串(url拿出來的是字串)
            $sql = "INSERT INTO `wishlist` (`id`, `member_id`, `list_id`) VALUES (NULL, ?, ?)";
        } else {
            $sql = "DELETE FROM `wishlist` WHERE `member_id`=? AND `list_id`=?";
        }
    };

    $stmt = $pdo->prepare($sql);

    $stmt->execute([
        $user_id,
        $sid
    ]);

    if(isset($_GET['add']) && $add) { //&&不能拿掉 因為第一次load網頁並沒有"add"
        $lastInsertId =  $pdo->lastInsertId();
        if(lastInsertId) {
            $result['success'] = true;
            $result['code'] = 201;
            $result['info'] = '成功插入資料';  
        }

    };

    // 影響的列數 (筆數)
    if($stmt->rowCount()==1){
        $result['success'] = true;
        $result['code'] = 200;
        $result['info'] = '成功取得資料';

    } else {
        $result['code'] = 404;
        $result['info'] = '找不到資料';
    }


};

echo json_encode($result, JSON_UNESCAPED_UNICODE);