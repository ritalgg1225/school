<?php

session_start();

if(! isset($_SESSION['compare'])){
    $_SESSION['compare'] = [];
}

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;
$qty = isset($_GET['qty']) ? intval($_GET['qty']) : 0;

if(! empty($sid)){
    // 如果 sid 不為 0, 有設定時

    if(empty($qty)) {
        // 移除
        unset($_SESSION['compare'][$sid]);

    } else {
        // 加入 或 變更
        // TODO: 要去資料庫確認有這個商品, 而且商品在架上

        $_SESSION['compare'][$sid] = $qty;
    }

}

echo json_encode($_SESSION['compare']);

header('Content-Type: text/plain');
print_r($_SESSION);