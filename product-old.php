<?php require __DIR__. './__connect_db.php' ?>
<?php 

$page_name='detail';
$params = [];

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;

if(isset($_GET['sid'])){
    $params['sid'] = $sid;
}

$item = isset($_GET['item']) ? intval($_GET['item']) : 0;
if(isset($_GET['item'])){
    $params['item']=$item;
}

$colorsid = isset($_GET['colorsid']) ? intval($_GET['colorsid']) : 0;
if(isset($_GET['colorsid'])){
    $params['colorsid']=$colorsid;
}

// $on = '';
// if(! empty($sid)){
//     $on .= " AND `product_sid` = $sid ";
// }
//要改成type_sid 先整理資料庫
$l_sql = "SELECT * FROM `lunggage_data` WHERE `SID`=". $sid;

$l_row = $pdo->query($l_sql)->fetch(PDO::FETCH_ASSOC);

if(empty($l_row)){
    // TODO:
}

// print_r($l_row);
// $uniq_name=[];
// $keyItem = " AND pl.`size`= $item AND cm.`color_sid`= $colorsid ";
$p_sql = "SELECT *, pl.sid product_list_sid  FROM `product_list` pl JOIN `color_mapping` cm ON pl.color_sid=cm.color_sid WHERE pl.`type_sid`=". $sid;
$p_rows = $pdo->query($p_sql)->fetchAll(PDO::FETCH_ASSOC);


// $b_sql = "SELECT p.*, c.* FROM `lunggage_data` p JOIN `product_color` c ON p.`SID` = c.`product_sid` $on";
// $b_stmt = $pdo->query($b_sql);

// $c_sql = "SELECT * FROM `lunggage_data` WHERE `cosi_id`=8";
// $c_stmt = $pdo->query($c_sql);

//品牌鎖定 值怎麼用前頁讀取 取Size
// $s_sql = "SELECT DISTINCT p.`type`, p.`SID`, c.`product_sid`,c.`size`, c.`color`, c.`cosi_id` FROM `lunggage_data` p JOIN `product_color` c ON p.`SID`=c.`product_sid` $on GROUP BY c.`product_sid` " ;
// $s_stmt = $pdo->query($s_sql);
// $sizeAll = $s_stmt->fetchAll(PDO::FETCH_ASSOC);

//品牌尺寸鎖定 值怎麼用前頁讀取 取顏色
// $o_sql ="SELECT p.`type`, p.`SID`, c.`product_sid`,  c.`size`, c.`color`, c.`cosi_id` FROM `lunggage_data` p JOIN `product_color` c ON p.`SID`=c.`product_sid` $on ";
// $o_stmt = $pdo->query($o_sql);
// $colorOne = $o_stmt->fetchAll(PDO::FETCH_ASSOC);

//推薦用
$r_sql ="SELECT p.*, c.`size_text`, c.`pic_nu`  FROM `lunggage_data` p JOIN `product_list` c
ON p.`SID` = c.`type_sid` ORDER BY RAND() LIMIT 5";
$r_stmt = $pdo->query($r_sql);
$random = $r_stmt->fetchAll(PDO::FETCH_ASSOC);

// header('Content-Type: text/plain');
// print_r($_SESSION);

// print_r($p_rows);
// exit;
?>
<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>商品展示頁</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="./css/style-wawa.css">
    <link rel="stylesheet" href="./css/slick.css">
    <link rel="stylesheet" href="./css/slick-theme.css">
    
<?php include __DIR__. './__navbar.php' ?>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Marko+One|Mukta+Malar:200,300,400,500,600,700,800|Noto+Sans+TC:100,300,400,500,700,900|Merriweather:300,400,700,900|Noto+Serif+TC:200,300,400,500,600,700,900');
        /* * {
            box-sizing: border-box;
        } */
        html {
            font-size: 16px;
        }
        body {
            /*
            font-family: 'Marko One', serif;
            font-family: 'Mukta Malar', sans-serif;
            font-family: 'Merriweather', serif;
            */
            font-family: 'Noto Sans TC', sans-serif;
            font-size: 1rem;
            color: #4d5258;
        }

        /* ---------------------------Wawa tamp */
        .con-1440 {
            max-width: 75%;
            margin: 0 auto;
        }
        .ff-merri{
            font-family: 'Merriweather', serif;
        }
        /* -------------------Rita tamplate special for prdoduct */
        .con-1200 {
            width: 65.5%;
            max-width: 90%;
        }
        .fisrt-size:first-child{
            border-bottom : 3px solid #4d5258;
        }
        .size-click{
            border-bottom: 3px solid #4d5258;
        }
        .circle_out{
            width: 26px;
            height: 26px;
            border-radius: 50%;
            /* border: 1px solid #4d5258; */
        }
        .circle_first:first-child{
            border: 1px solid #4d5258;
        }
        .circle_click{
            border: 1px solid #4d5258;
        }
        .circle{
            width: 20px;
            height: 20px;
            border-radius: 50%;
            border: 1px solid rgb(255, 255, 255);
            /* background: #818E9B; */
        }
        .margin-right20{
            margin-right:20px; 
        }
        .gold-text{
            color: #cfb06d;
        }
        .full-darkbg{
            background: #E1E1E1;
        }
        .full-lightbg{
            background: #F9F6F1;
        }
        
        /*--------------------------------------product wrap*/
        .breadcrumbs{
            padding: 15px;
        }
        .product_picouter{
            width: 50%;
            flex-direction: column;
            margin-right: 50px;
        }
        .product_pic img{
            width: 100%;
        }
        
        .product_info {
            width: 50%;
            border: 1px solid #ccc;
            padding: 20px;
        }
        .product_name {
            border-bottom: 1px solid #ccc;
            justify-content: space-between;
            align-items: center;
        }
        .product_name p{
            margin: 20px 0;
        }
        .product_name a{
            width: 30px;
            height: 30px;
        }
        .product_name img{
            width: 100%;
            object-fit: cover;
        }
        /* ---------------此區slick 樣式 */
        .product_picouter>.slick-prev{
            left: 20px;
        }
        /* ------------------------尺寸5種img */
        .product_size ul{
            align-items: flex-end;
        }
        .size-xs{
            width: 25px;
            height: 25px;
        }
        .size-s{
            width: 30px;
            height: 30px;
        }
        .size-m{
            width: 35px;
            height: 35px;
        }
        .size-l{
            width: 40px;
            height: 40px;
        }
        .size-xl{
            width: 45px;
            height: 45px;
        }

        /* ------------------商品右側版 */
        .product_name{
            font-size: 1.8rem;
        }
        .product_size li{
            padding-bottom: 5px;
            /* border-bottom:3px solid #fff; */
        }

        .product_size a{
            color: #4d5258;
        }
        .product_intro{
            margin-top: 10px;
            line-height: 1.7rem;
        }
        .product_num{
            margin-top: 20px;
        }
        .product_num>.d-flex{
            margin-top: 10px;
        }
        .qty{
            border: 1px solid #ccc;
            border-radius: 3px;
            width: 22px;
            text-align: center;
            margin: 0 5px;
        }
        .product_num input{
            width: 50px;
        }
        .storage{
            margin-left: 20px;
        }

        .product_price{
            text-align: right;
            color: #9b2525;
            font-size: 2rem;
        }
        .product_btn .btn {
            width: 100%;
            height: 30px;
            border: 1px solid #818E9B;
            border-radius: 1.6px;
            padding: 5px;
            margin: 16px auto;
        }

        .product_btn .btn1 {
            margin-top: 33px;
            background: #818E9B;
            color: #fff;
        }
        /* -----------------------information */
        .middle_border{
            padding-top: 80px;
            padding-bottom: 80px;
        }
        .for-line{
            border-bottom: 1px solid #cfb06d;
            font-size: 2.5rem;
            margin:10px 0 30px 0;
            font-weight: 900;
        }
        .middle_border p{
            margin:0 0 10px 0;
        }
        .article_infor>.inner_text{
            padding:0 80px 0 80px;
            font-size: 0.9rem;
            line-height: 1.5rem;
        }
        .detail_img{
            width: 45%;
            /* max-width: 500px; */
            /* height: 400px; */
            margin: 8px;
        }
        .product_detail img{
            width: 100% ;
            object-fit: cover;
        }
        .detail_group{
            /* padding-top: 30px; */
            flex-wrap: wrap;
            
        }
        /* -------------------specifications */
        .left_lung{
            width: 66%;
            justify-content: center;
        }
        .left_lung img{
            width: 75%;
        }
        .right_lung{
            width: 33%;
            background: #838F9B;
            border-radius: 10px;
        }
        .right_lung li{
            color: #F9F6F1;
            line-height: 3rem;
        }
        /* ----------------product_rule */
        .product_rule{
            color: #F9F6F1;
            padding-top: 80px;
            font-size: 0.9rem;
        }
        .full-imagebg{
            background: url("./images/product_rule.jpg") center bottom no-repeat;
            background-size: cover;
            height: 400px;
            z-index: -1;
        }
        .cover-bg{
            height: 400px;
            background: rgba(0, 0, 0, .6);
            z-index: 1;
            flex-direction: column;
        }
        /* -------------slider bar */
        .slider_bar{
            padding:  50px 0;
            /* overflow: hidden; */
            align-content: center;
        }
        .like_pic{
            text-align: center;
        }
        .like{
            margin: 10px 0;
        }
        .slick-slide .like_pic img{
            width: 150px;
            height: 150px;
            margin: 0 auto;
        }
        /* ------------------------slick 樣式 */

        .slick-arrow::before{
            color: #4d5258;
            font-size: 1.5rem;
        }
        .slider_bar>.slick-prev, .slider_bar>.slick-next{
            top: 50%;
        }

        /* -------------------------------------------------detail RWD */
        @media screen and (max-width:1120px) {
            .con-1200{
                width: 100%;
            }
            .product_rule ul {
                padding: 0 30px;
            }
        }
            @media screen and (max-width:830px) {
            .product_info{
                padding:10px 20px;
            }
            .middle_border{
                padding-top: 50px;
                padding-bottom: 50px;
            }
            .con-1200{
                    max-width: 95%;
                    padding: 10px;
            }
            
            
            @media screen and (max-width:767px){
                .product_wrap {
                    flex-direction: column;
                    align-items: center;
                }
                .product_picouter{
                    width: 90%;
                    margin-right: 0;
                }
                .product_info {
                    width: 95%;
                }
            }
            @media screen and (max-width:630px){
                .right_lung ul{
                    padding:0 30px;
                }
                /* -----細節 */
                .detail_img {
                    width: 90%;
                }
            }
            @media screen and (max-width:583px){
                .right_lung ul{
                    padding:0 20px;
                }
            }
            @media screen and (max-width:519px){
                /* ----detail nav重疊 */
                .container{
                    position: relative;
                    top: 60px;
                    max-width: 100%;
                    padding: 0;
                }

                .product_btn .btn1{
                    margin-top: 40px;
                }
                .product_btn .btn{
                    width: 100%;
                    height: 40px;
                    line-height: 30px;
                    margin-bottom: 0;
                }
                /* ----商品上方文字區 */
                .product_info{
                    border: none;
                }
                /* ----特色區塊 */
                .spec_group {
                    flex-direction: column;
                    align-items: center;
                }
                .left_lung{
                    width: 100%;
                }
                .left_lung img{
                    width: 100%;
                }
                .right_lung{
                    width: 100%;
                }
                .right_lung ul{
                    padding:0 50px;                    
                }
                .right_lung li{
                    line-height: 2rem;
                }
                /* -----slider 樣式 */
                .slick-next{
                    right: 0;
                }
                .slick-prev{
                    left: -10px;
                    z-index: 5;
                }

            }
            
        }


    </style>
</head>

<body>
    <div class="container con-1200 mairgin-0auto">
        <div class="breadcrumbs">
            <a class="nonstyle-a" href="">首頁></a>
            <a class="nonstyle-a" href="">品牌></a>
            <a class="nonstyle-a" href="">型號</a>
        </div>
        <?php //$rowAll = $b_stmt->fetch(PDO::FETCH_ASSOC) ?>
        <div class="product_wrap d-flex" data-sid="">
            <!-- 點選更換圖片區 start -->
            <div class="product_picouter d-flex justify-center">
            <div class="product_pic relative">

            <!-- <div><img id="product_image" src="" alt=""></div> -->
<?php/*
            <?php foreach($p_rows as $sliderimg): ?>
        <?php if($sliderimg['sid']==$sid){?>
            <?php $images =  $sliderimg['imgs'] ;
                $this_images = explode(";;", $images);
                for($i=0; $i<count($this_images) ; $i++):
             ?>
            <?= '<div><img src="./images/product/' . $this_images[$i] . '"alt=""></div>' ?>
                <?php endfor; 
                }
                    endforeach;?>
*/?>
            </div>
        </div>
            <!-- 點選更換圖片區 end -->
            <!-- 商品名稱 尺寸 & 詳細說明 start -->
            <div class="product_info relative">
                <div class="product_name d-flex">
                    <p><?= $l_row['brand'] ?></p>
                    <a class="wish" href=""><img id="heartShape" src="./images/icon-love-1.svg" alt=""></a>
                </div>
                <div class="product_intro">
                    <ul class="nonstyle-ul p-0 m-0">
                        <li><?= $l_row['type'] ?></li>
                        <li><?= $p_rows[0]['size_text'] ?></li>
                        <li><?= $l_row['roll'] ?></li>
                    </ul>
                </div>
                <!-- 商品名稱 尺寸 & 詳細說明 end -->
                <!-- 尺寸區 start TODO::php某項商品的所有尺寸 -->
                <div class="product_size">
                    <p>尺寸</p>
                    <ul class="nonstyle-ul d-flex p-0 m-0">
                    <?php $property_types = [];
                    foreach($p_rows as $size){ ?>
                    <?php if( in_array($size['size'],$property_types)){ 
                        continue;
                            } $property_types[]= $size['size'];
                        ?>
                        <?php if ($item==0): ?>
                        <li class="margin-right20 text-center fisrt-size click-size" data-key="<?= $size['size'] ?>"><a class="nonstyle-a" href="javascript:" data-key="<?= $size['size'] ?>"><img class="size-xs" src="./images/icon_luggage.svg" alt="">
                            <p class="p-0 m-0"><?= $size['size_text'] ?></p>
                            </a></li>
                        <?php elseif($item==$size['size']): ?>
                        <li class="margin-right20 text-center size-click click-size" data-key="<?= $size['size'] ?>"><a class="nonstyle-a" href="javascript:" data-key="<?= $size['size'] ?>"><img class="size-xs" src="./images/icon_luggage.svg" alt="">
                            <p class="p-0 m-0"><?= $size['size_text'] ?></p>
                            </a></li>
                        <?php else: ?>
                        <li class="margin-right20 text-center click-size" data-key="<?= $size['size'] ?>"><a class="nonstyle-a" href="javascript:" data-key="<?= $size['size'] ?>"><img class="size-xs" src="./images/icon_luggage.svg" alt="">
                            <p class="p-0 m-0"><?= $size['size_text'] ?></p>
                            </a></li>
                        <?php endif; ?>
                                             
                <?php  }?>
                    
                    </ul>
                </div>
                <!-- 尺寸區 end -->
                <!-- 顏色區 start TODO::php某項商品的所有顏色 -->
                <div class="product_color">
                    <p>顏色</p>
                    <ul class="nonstyle-ul d-flex p-0 m-0">

                    <?php foreach($p_rows as $k=>$color){ ?>
                    <?php if( in_array($color['color_sid'],$property_types)){ 
                        continue;
                            } $property_types[]= $color['color_sid'];
                        ?>
                        <div class="circle_out d-flex justify-center align-item-center margin-right20 <?= $k==0 ? 'circle_click' : '' ?>">
                        <a class="nonstyle-a click-color" href="javascript:sel_color(<?=$k?>)" data-key="<?= $color['color_sid'] ?>"><li class="circle" data-key="<?= $color['color_sid'] ?>" style="background:<?= $color['color_code']  ?>"></li></a>
                        </div>
                        <?php  } ?>

                    <?php /*
                    <?php $property_types = [];
                    foreach($p_rows as $color){ ?>
                    <?php if( in_array($color['color_sid'],$property_types)){ 
                        continue;
                            } $property_types[]= $color['color_sid'];
                        ?>
                        <?php if ($colorsid==0): ?>
                        <div class="circle_out d-flex justify-center align-item-center margin-right20 circle_first">
                        <a class="nonstyle-a click-color" href="#" data-key="<?= $color['color_sid'] ?>"><li class="circle" data-key="<?= $color['color_sid'] ?>" style="background:<?= $color['color_code']  ?>"></li></a>
                        </div>
                        <?php elseif($colorsid==$color['color_sid']): ?>
                        <div class="circle_out d-flex justify-center align-item-center margin-right20 circle_click">
                        <a class="nonstyle-a click-color" href="#" data-key="<?= $color['color_sid'] ?>"><li class="circle" data-key="<?= $color['color_sid'] ?>" style="background:<?= $color['color_code']  ?>"></li></a>
                        </div>
                        <?php else: ?>
                        <div class="circle_out d-flex justify-center align-item-center margin-right20">
                        <a class="nonstyle-a click-color" href="#" data-key="<?= $color['color_sid'] ?>"><li class="circle" data-key="<?= $color['color_sid'] ?>" style="background:<?= $color['color_code']  ?>"></li></a>
                        </div>
                        <?php endif; ?>
                        <?php  } ?>
                        */ ?>
                    </ul>
                </div>
                <!-- 顏色區 end -->
                <!-- 數量區 start -->
                <div class="product_num">
                    數量
                    <div class="d-flex">
                    <button class="qty qty_decrese">-</button>
                    
                    <input name="productQty" class="productQty text-center" type="text" value="1">
                    <button class="qty qty_increse">+</button>
                    </div>
                </div>
                <!-- 數量區 end -->
                <div class="product_price" data-price="<?= $l_row['price']?>">NT$<?= $l_row['price']?></div>
                <div class="product_btn">
                    <button class="btn btn1 text-center add_to_cart_btn">加入購物車</button>
                        <?php if( count($_SESSION['compare']) >2 ): ?>
                            <button class="btn text-center" disabled=disabled >比較清單已達上限</button>
                        <?php else: ?>
                        <button class="btn text-center add_to_compare_btn">加入比較清單</button>
                        <?php endif; ?>
                </div>

            </div>
        </div>
        <div class="middle_border">
            <div class="article_infor">
                <div class="for-line">
                    <p class="gold-text text-center ff-merri">INFORMATION</p>
                </div>
                <p class="inner_text">
                    <?= $l_row['intro'] ?>
                </p>
            </div>
        </div>
    </div>
    <div class="full-darkbg">
        <div class="middle_border con-1200 mairgin-0auto">
            <div class="product_detail">
                <div class="for-line">
                    <p class="gold-text text-center ff-merri">PRODUCT DETAIL</p>
                </div>
                <div class="detail_group d-flex justify-center">
                    <div class="detail_img">
                        <img src="./images/1.png" alt="">
                    </div>
                    <div class="detail_img">
                            <img src="./images/2.png" alt="">
                    </div>
                    <div class="detail_img">
                            <img src="./images/3.png" alt="">
                    </div>
                    <div class="detail_img">
                            <img src="./images/4.png" alt="">
                    </div>
                    <div class="detail_img">
                            <img src="./images/5.png" alt="">
                    </div>
                    <div class="detail_img">
                            <img src="./images/6.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-lightbg">
        <div class="middle_border con-1200 mairgin-0auto">
            <div class="spec">
                <div class="for-line">
                    <p class="gold-text text-center ff-merri">SPECIFICATIONS</p>
                </div>
                <div class="spec_group d-flex justify-center">
                    <div class="left_lung align-item-center d-flex">
                        <img src="./images/img-luggage-sketch.svg" alt="">
                    </div>
                    <div class="right_lung">
                        <ul class="nonstyle-ul">
                            <div class="d-flex">
                                <li class="margin-right20">款式</li>
                                <li><?= $l_row['border'] ?></li>
                            </div>
                            <div class="d-flex">
                                <li class="margin-right20">顏色</li>
                                <li><?= $p_rows[0]['color'] ?></li>
                            </div>
                            <div class="d-flex">
                                <li class="margin-right20">材質</li>
                                <li><?= $l_row['texture'] ?></li>
                            </div>
                            
                            <div class="d-flex">
                                <li class="margin-right20">尺寸</li>
                                <li><?= $p_rows[0]['size_text'] ?></li>
                            </div>

                            <div class="d-flex">
                                <li class="margin-right20">重量</li>
                                <li><?= $l_row['kg'] ?></li>
                            </div>
                            <div class="d-flex">
                                <li class="margin-right20">容量</li>
                                <li><?= $l_row['insideL'] ?></li>
                            </div>
                            <div class="d-flex">
                                <li class="margin-right20">保固</li>
                                <li><?= $l_row['warranty'] ?></li>
                            </div>
                        </ul>
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="full-imagebg relative">
        <div class="cover-bg relative d-flex">
            <div class="product_rule mairgin-0auto">
                <ul class="nonstyle-ul">
                    <li class="inner_text">出貨與配送</li>
                    <li>1.在遵守本協議書的前提下，我方將為您供應訂單確認函所列的商品。我方將於5-7個工作日送達，但無法保證一定能在特定日期送達。</li>
                    <li>2.實際到貨時間取決於Samsonite所委託第三方物流供應商之運送時程，而第三方物流之運送時程不時會有變動，恕不另行通知。</li>
                    <li>3.我方委託第三方物流進行送貨。貨物送達時應有人負責簽收。若您無法簽收時，您同意由收件地址有辨別事理能力之代理人代為簽收。</li>
                </ul>
                <ul class="nonstyle-ul">
                    <li class="inner_text">退貨與退款處理</li>
                    <li>1.網購商品時，可在到貨七天內退貨。退貨及退款規範請參閱退款與退換貨。</li>
                    <li>2.如欲辦理退貨，請致電客服專線0800-088-349 轉電商客服中心，時間為週一至週五(例假日除外)，上午9點至下午5點，由專人為您服務或來信。</li>
                </ul>
                <ul class="nonstyle-ul">
                    <li class="inner_text">其他</li>
                    <li>1.6折(含)以下特價商品，恕不提供免費保修服務。</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="recommend_border con-1200 mairgin-0auto">
        <div class="recommend">
            <div class="for-line ">
                <p class="gold-text text-center ff-merri">YOU MAY ALSO LIKE</p>
            </div>
            <div class="slider_bar d-flex">
            <?php foreach($random as $like): ?>
                    <?php if($like['brand']!=''): ?>
                <div class="like_pic">
                        <img src="./images/product/<?= $like['pic_nu'] ?>" alt="">
                        <div class="like"><?= $like['brand'] ?><?= "</br>". $like['type'] ?><?= " ". $like['size_text'] ?></div>
                </div>
                <?php else: //除錯用 ?>
                
                    <p>沒有取到資料 請除錯</p>
                        <?php endif; ?>
                        <?php endforeach; ?>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="./js/__nav.js"> </script>
    <script src="./js/slick.min.js"></script>
    <script src="./js/detectmobilebrowser.js"></script>
    <script>
    var $p_rows = <?= json_encode($p_rows); ?>;
    var current_item;
    var product_sid;
    // function imgSelect(){
    //         var imgs_array = current_item['imgs'].split(";;") ;
    //         for(i=0 ; i<imgs_array.length ; i++){
    //         $(".product_pic").append('<img src="./images/product/'+ imgs_array[i]+ '">');
    //         };
    //     };
    function sel_color(index){
        current_item = $p_rows[index];
        // $('#product_image').attr('src', './images/product/' + current_item['pic_nu'] );
    }
    sel_color(0);
    // function changeImg(){
    //     var sid = ;
    //         $.get('add_to_wishlist_api.php',{sid:product_sid}, function(data){
    //             if(data.success) {
    //                 changeWishlistIcon(true); 
    //             } else {
    //                 changeWishlistIcon(false); 
    //             }
    //         }, 'json');
    
        //尺寸選擇     
        // $('.product_size li').click(function(){
        //     $(this).siblings().css('border-bottom','3px solid #fff');
        //     $(this).css('border-bottom','3px solid #4d5258');
        // });

        //顏色選擇
        $('.circle').click(function(){
            var sid = current_item['product_list_sid'];
            $(this).closest('.circle_out').siblings().css('border','1px solid #fff');
            $(this).closest('.circle_out').css('border','1px solid #4d5258');
            console.log(sid);
            $(".product_wrap").attr('data-sid',sid);
        });

        //上方商品
        $('.product_pic').slick();

        //數量增減
        var amount = $('.productQty');
        var inputValue = parseInt($('.qty_increse').closest('.product_num').find('input').val());
        function countPrice (){
            var subprice = inputValue * parseInt($('.product_price').attr('data-price'));
            $('.product_price').html('NT$' + subprice);
        }
        $('.qty_increse').click(function(){
            if(inputValue < 10 ){            
                inputValue = parseInt(inputValue + 1);
                amount.attr('value',inputValue);
            }
            countPrice();
        });
        $('.qty_decrese').click(function(){
            if(inputValue > 1){
                inputValue = parseInt(inputValue - 1);
                amount.attr('value',inputValue);
            }
            countPrice();
        });
        
        //加上手機版
        if(jQuery.browser.mobile){
            $('.slider_bar').slick({
                infinite: false,
                slidesToShow: 2,
                slidesToScroll: 2});
            } else {
                $('.slider_bar').slick({
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 3});
        };

        $('.add_to_compare_btn').click(function(){
            var card = $(this).closest('.product_wrap');
            var sid = card.attr('data-sid');
            var item = $('.size-click').attr('data-key');
            var colorsid = $('.circle_click>a').attr('data-key');
            

            $.get('./add_to_compare_api.php', {sid:sid, item:item, colorsid:colorsid}, function(data){
                alert('您已加入比較清單');
                
            }, 'json');
        });

//加入購物車 比較 願望清單開始

        //點擊加入購物車
        $('.add_to_cart_btn').click(function(){
            var card = $('.product_wrap');
            var qty = card.find('input').val();

            $.get('add_to_cart_api.php', {sid:product_sid, qty:qty,add:true}, function(data){
                // alert('商品加入購物車');
                // alert(sid + "::" + qty);
                cart_count(data);//泡泡選單
            },'json');
        });
        //點擊加入比較
        // $('.add_to_compare_btn').click(function(){
            // alert(sid + "加入比較");

            // $.get('./add_to_compare_api.php', {sid:sid}, function(data){
                // alert('您已加入比較清單');
        //         compare_count(data);//泡泡選單
        //     }, 'json');
        // });

        //加入願望清單
        function changeWishlistIcon(inWishList){
            var image = inWishList ? "./images/icon-love-2.svg" : "./images/icon-love-1.svg"; 
            $("#heartShape").attr("src",image); 
        };

        function checkWishList(){
            $.get('add_to_wishlist_api.php',{sid:product_sid}, function(data){
                if(data.success) {
                    changeWishlistIcon(true); 
                } else {
                    changeWishlistIcon(false); 
                }
            }, 'json');   
        };
        // if (isset($_SESSION['user'])){
        //     checkWishList();  
        // };
      
        $('#heartShape').click(function(){
            var add = ($("#heartShape").attr("src") == "./images/icon-love-1.svg")? true: false;
            $.get('add_to_wishlist_api.php',{sid:product_sid,add:add}, function(data){
                if(data.success) {
                   changeWishlistIcon(add); 
                }
            }, 'json');
        });
//加入購物車 比較 願望清單結束
        
        //變更尺寸
/*
        $('.click-size').click(function(){
            
            $('.product_size li:first-child').css('border-bottom','transparent');
            var colorsid = $('.circle_click>a').attr('data-key') ;
            var sizeID = $(this).attr('data-key');
            $(this).find('a').attr('href', "?sid=<?= $size['type_sid'] ?>&item="+sizeID+"&colorsid="+colorsid);
            console.log("KEY = " + sizeID);
        });
*/
            
/*
            //點擊顏色換圖片
             $('.click-color').click(function(){
                 $('.circle_out:first-child').css('border','transparent');
                 //get size id
                var sizeID =  $('.size-click').attr('data-key');
console.error('sizeID :' + sizeID);
// debugger;
                 var colorsid = $(this).attr('data-key');
                 $(this).attr('href', "?sid=<?= $size['type_sid'] ?>&item="+ sizeID +"&colorsid="+colorsid);
                 console.log("KEY = " + colorsid);
             });

*/
        



    </script>
</body>

</html>